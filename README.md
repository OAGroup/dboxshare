# dboxShare 开源企业网盘系统

基于 .NET 的开源企业级网盘系统，提供简便易用、安全可靠的文件云存储、云管理、云共享和云协作解决方案。


## 主要功能特点

* 支持 SQL Server 和 MySQL 两大主流数据库
* 用户账号禁止多点登录
* 站点独立安全密匙
* 多语言用户界面切换(支持扩充语言包)
* AES256 银行级算法加密文件
* 共享文件夹权限管控(支持根据部门、角色、用户进行授权)
* 多功能文件上传(支持大文件分片上传、拖放上传、文件夹上传)
* 文件历史版本管理
* 在线预览多种文件格式(支持自定义扩展)
* 压缩包在线查看及解压
* 文件任务工作流
* 文件双向同步
* 文件快速全文检索
* 文件操作日志记录


## 软件信息

开发语言：C# / .NET / JavsScript / CSS / HTML

运行环境：Windows Server + IIS + .NET Framework

 数据库：SQL Server / MySQL

开源协议：AGPL v3.0


## 安装说明

安装部署说明请参考 http://www.dboxshare.com/install.html


## 联系方式

官方网站：http://www.dboxshare.com/

电子邮箱：support@dboxshare.com

QQ：3521670882

欢迎与我们联系进行咨询、建议和反馈